<?php

use Illuminate\Support\Facades\Route;
//Backend
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\OrderController;

//Frontend
use App\Http\Controllers\Frontend\IndexController as FrontendIndex;
use App\Http\Controllers\Frontend\LoginController as FrontendLogin;
use App\Http\Controllers\Frontend\CartController as FrontendCart;
use App\Http\Controllers\Frontend\CheckoutController as FrontendCheckout;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/starter', function () {
//    return view('backend.starter');
//});
//
//Route::get('/dashboard', function () {
//    return view('backend.dashboard');
//});
//
//Route::get('/products', function () {
//    return view('backend.products.dashboard');
//});
//Route::get('/products/create', function () {
//    return view('backend.products.create');
//});
//Route::get('/login', function () {
//    return view('backend.auth.login');
//});

Route::get('/', [FrontendIndex::class, 'index'])->name('home');
Route::get('/login', [FrontendLogin::class, 'login'])->name('login');
Route::post('/login', [FrontendLogin::class, 'authenticate']);
Route::get('/logout', [FrontendLogin::class, 'logout'])->name('logout');

Route::get('/cart', function () {
    return view('frontend.cart.cart');
})->name('cart.show');

Route::get('/checkout', function () {
    return view('frontend.payment.checkout');
})->name('checkout.show');
Route::get('/checkout/result', function () {
    return view('frontend.payment.result');
})->name('checkout.result');

Route::middleware('auth')->group(function () {
    Route::post('cart/add', [FrontendCart::class, 'add'])->name('cart.add');
    Route::get('cart/remove/{cart}', [FrontendCart::class, 'remove'])->name('cart.remove');
    Route::post('cart/update/{cart}', [FrontendCart::class, 'update'])->name('cart.update');
    Route::post('checkout', [FrontendCheckout::class, 'store'])->name('checkout.store');
});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('login', [LoginController::class, 'login'])->name('login');
    Route::post('login', [LoginController::class, 'authenticate']);

    Route::middleware('check.role')->group(function () {
        Route::get('/', [DashboardController::class, 'index']);
        Route::get('logout', [LoginController::class, 'logout'])->name('logout');
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
        //Products
        Route::resource('products', ProductController::class);
        Route::resource('orders', OrderController::class);
        Route::post('orders/updateStatus', [OrderController::class,'updateStatus'])->name('orders.updateStatus');
    });
});
