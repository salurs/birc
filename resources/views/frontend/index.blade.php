@extends('frontend.layouts.master')

@section('content')
    <!-- Start Product Area -->
    <div class="product-area section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Trending Item</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="product-info">
                        <div class="row">
                            @foreach($products as $product)
                            <div class="col-xl-3 col-lg-4 col-md-4 col-12">
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="product-details.html">
                                            <img class="default-img" src="{{$product->image}}" alt="#">
                                            <img class="hover-img" src="{{$product->image}}" alt="#">
                                        </a>
                                        <div class="button-head">
                                            <form action="{{ route('cart.add') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="product" value="{{ $product->id }}">
                                                <div class="product-action">
                                                    <div>
                                                        <input name="quantity" type="number" min="1" step="1" value="1" class="form-control" style="width: 75px;box-shadow: none;">
                                                    </div>
                                                </div>
                                                <div class="product-action-2">
                                                    {{-- <a title="Add to cart" href="#">Add to cart</a> --}}
                                                    <button class="p-2">Add to cart</button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <h3><a href="#">{{$product->title}}</a></h3>
                                        <div class="product-price">
                                            <span>{{$product->price}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Product Area -->
@endsection
@section('styles')@endsection
@section('scripts')@endsection
