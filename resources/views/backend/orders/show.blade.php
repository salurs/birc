@extends('backend.layouts.master')
@section('page-nav')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Sipariş
                        <small>Detay</small>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}" class="text-orange nav__link">Anasayfa</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.orders.index')}}" class="text-orange nav__link">Liste</a></li>
                        <li class="breadcrumb-item active">Detay</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <small class="float-right">Tarih: {{setDateFormat($order->created_at)}}</small>
                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Sipariş Bilgisi</b><br>
                                <br>
                                <b>Sipariş No</b> {{$order->order_no}}<br>
                                <b>Ödeme Türü</b> {{getPaymentType($order->payment_type)}}<br>
                                <b>Durum</b> {{getOrderStatus($order->status)}}<br>
                                <b>Toplam Tutar</b> {{$order->total}}<br>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Adres Bilgisi</b><br>
                                <br>
                                <b>Adres Adı</b> {{$order->address->title}}<br>
                                <b>Adı Soyadı</b> {{$order->address->name}}<br>
                                <b>Email</b> {{$order->address->email}}<br>
                                <b>Telefon</b> {{$order->address->phone}}<br>
                                <b>Adres</b> {{$order->address->address}}<br>
                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Kullanıcı Bilgisi</b><br>
                                <br>
                                <b>Adı Soyadı</b> {{$order->user->name}}<br>
                                <b>Email</b> {{$order->user->email}}<br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Görsel</th>
                                        <th>Adet</th>
                                        <th>Ürün</th>
                                        <th>Fiyat</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order->orderItems as $item)
                                    <tr>
                                        <td><img height="50" src="{{$item->product->image}}" alt="image"></td>
                                        <td>{{$item->quantity}}</td>
                                        <td>{{$item->product_name}}</td>
                                        <td>{{$item->price}}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <hr/>
                        <div class="row">
                            <!-- accepted payments column -->
                            <!-- /.col -->
                            <div class="col-12">
                                <div class="float-right">
                                    <strong>Toplam:</strong> <span class="badge badge-success">{{$order->total}}</span>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('styles')
@endsection
@section('scripts')
@endsection

