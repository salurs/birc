@extends('backend.layouts.master')
@section('page-nav')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Şipariş
                        <small>Liste</small>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}" class="text-orange nav__link">Anasayfa</a></li>
                        <li class="breadcrumb-item active">Liste</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="10">#</th>
                                <th>Şipariş No</th>
                                <th>Kullanıcı Adı</th>
                                <th>Ödeme Türü</th>
                                <th>Toplam</th>
                                <th>Durum</th>
                                <th width="10">İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr id="item-{{$order->id}}">
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$order->order_no}}</td>
                                    <td>{{$order->user->name}}</td>
                                    <td>{{getPaymentType($order->payment_type)}}</td>
                                    <td>{{$order->total}}</td>
                                    <td>
                                        <select name="status" class="form-control status" data-order="{{$order->id}}">
                                            <option value="0" {{$order->status == 0 ? 'selected' : ''}}>Beklemede</option>
                                            <option value="1" {{$order->status == 1 ? 'selected' : ''}}>Onaylandı</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{route('admin.orders.show',$order->id)}}" title="Göster" class="btn btn-info">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('styles')

@endsection
@section('scripts')
    <script>
        $('.status').on('change',function () {
            $.ajax({
                type:'post',
                url:'{{route("admin.orders.updateStatus")}}',
                data:{
                    status: $(this).val(),
                    id: $(this).data('order')
                },
                success:function(res){
                    if(res){
                        alertify.success('Durum güncellendi');
                    }else{
                        alertify.error('Durum güncellenemedi');
                    }
                }
            });
        })
    </script>
@endsection
