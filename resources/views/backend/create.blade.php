@extends('backend.layouts.master')
@section('page-nav')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Ürün
                        <small>Ekle</small>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#" class="text-orange nav__link">Anasayfa</a></li>
                        <li class="breadcrumb-item"><a href="#" class="text-orange nav__link">Liste</a></li>
                        <li class="breadcrumb-item active">Ekle</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- ./row -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-olive card-outline card-tabs">
                        <!-- /.general -->
                        <div class="card-body">
                            <form action="#" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Ürün Kategori</label>
                                    <select name="category_id" class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;" required>
                                        <option value="">Seçim yapın</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Adı</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control" name="name" value="{{old('name')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Açıklama</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <textarea id="body" name="body" class="form-control editors">{!! old('body') !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Detay</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <textarea id="detail" name="detail" class="form-control editors">{!! old('detail') !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Title</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control" name="title" value="{{old('title')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control" name="description" value="{{old('description')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Keywords</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control" name="keywords" value="{{old('keywords')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Görseller<span class="text-danger small">(Türü: jpeg, png, jpg, gif, svg olmalı ve Max: 1MB olmalıdır.)</span></label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input id="images" type="file" name="images[]" class="form-control" multiple="multiple"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-image mb-3 text-center">
                                    <div class="imgPreview"> </div>
                                </div>
                                <hr>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-olive">Kaydet</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('styles')
    <style>
        .imgPreview img{max-height: 150px; border:1px solid #ddd; margin:5px;}
    </style>
@endsection
@section('scripts')
    <script>
        $(function() {
            // Multiple images preview with JavaScript
            var multiImgPreview = function(input, imgPreviewPlaceholder) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#images').on('change', function() {
                multiImgPreview(this, 'div.imgPreview');
            });
        });
    </script>
@endsection

