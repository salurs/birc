
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-olive elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{asset('assets/backend/dist/img/AdminLTELogo.png')}}" alt="logo" class="brand-image elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Bircom</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Admin</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!--Dashboard-->
                <li class="nav-item">
                    <a href="{{route('admin.dashboard')}}" class="nav-link {{ (request()->is('admin/dashboard')) ? 'active' : '' }}"><i class="nav-icon fas fa-th-large"></i><p>Anasayfa</p></a>
                </li>
                <!--Orders-->
                <li class="nav-item">
                    <a href="{{route('admin.orders.index')}}" class="nav-link {{ (request()->is('admin/orders*')) ? 'active' : '' }}"><i class="nav-icon fas fa-cart-plus"></i><p>Siparişler</p></a>
                </li>
                <!--Products-->
                <li class="nav-item has-treeview {{ (request()->is('admin/products*')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('admin/products*')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tags"></i>
                        <p>Ürünler <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview pl-3">
                        <li class="nav-item">
                            <a href="{{route('admin.products.index')}}" class="nav-link"><i class="fas fa-list-ol nav-icon"></i><p>Liste</p></a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.products.create')}}" class="nav-link"><i class="fas fa-plus-square nav-icon"></i><p>Ekle</p></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
