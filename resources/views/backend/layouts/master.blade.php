<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin</title>


    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <!-- alertify -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/alertify/alertify.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/alertify/default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/alertify/semantic.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/alertify/bootstrap.min.css')}}">
    <!-- jquery-ui -->
    <link rel="stylesheet" href="{{asset('assets/backend/plugins/jquery-ui/jquery-ui.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/backend/dist/css/adminlte.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('styles')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @include('backend.layouts.navbar')
    @include('backend.layouts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('page-nav')
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    @include('backend.layouts.footer')
    @include('backend.partials.alertify')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('assets/backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('assets/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('assets/backend/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('assets/backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- alertify -->
<script src="{{asset('assets/backend/plugins/alertify/alertify.min.js')}}"></script>
<!-- jquery-ui -->
<script src="{{asset('assets/backend/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- ckeditor -->
<script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/backend/dist/js/adminlte.min.js')}}"></script>
<script>
    $(()=>{
        // ajaxSetup
        $(function(){
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        $('#example1_paginate').on('click',function(){
            $("input[data-bootstrap-switch]").each(function(){
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
        });
        // on - off
        $(function () {
            $("input[data-bootstrap-switch]").each(function(){
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
        });
        //datatable set lang
        $(function () {
            $("#example1").DataTable();
        });
        //Initialize Select2 Elements
        $('.select2').select2();



    });
    //CKEditor
    const allEditors = document.querySelectorAll('.editors');
    for (let i = 0; i < allEditors.length; ++i) {
        CKEDITOR.replace( allEditors[i]);
    }
    //CKEDITOR.config.autoParagraph = false;

    function deleteItem(route,id,item='#item-') {
        alertify.confirm("Kalıcı olarak silmek istediğinize emin misiniz?","Bu işlem geri alınamaz",()=>{
            $.ajax({
                type:'delete',
                url:route+'/'+id,
                success:function(res){
                    if(res){
                        $(item + id).remove();
                        alertify.success('Kayıt Silindi.');
                    }else{
                        alertify.error('Kayıt Silinemedi.');
                    }
                }
            });
        },()=>{
            alertify.error("Silme işlemi iptal edildi.");
        });
    }
</script>
@yield('scripts')
</body>
</html>
