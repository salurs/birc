@extends('backend.layouts.master')
@section('page-nav')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Ürün
                        <small>Liste</small>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}" class="text-orange nav__link">Anasayfa</a></li>
                        <li class="breadcrumb-item active">Liste</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h3 class="card-title float-none">
                            <a href="{{route('admin.products.create')}}" class="btn btn-dark">Ürün Ekle</a>
                            <div class="card-tools float-right">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="10">#</th>
                                <th>Görsel</th>
                                <th>Adı</th>
                                <th>Adı</th>
                                <th width="10">İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr id="item-{{$product->id}}">
                                    <td>{{$loop->iteration}}</td>
                                    <td><img height="50" src="{{$product->image}}" alt="image"></td>
                                    <td>{{$product->title}}</td>
                                    <td>{{$product->price}}</td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{route('admin.products.edit',$product->id)}}" title="Düzenle" class="btn btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a id="{{$product->id}}" href="javascript:void(0);" title="Sil" class="btn btn-danger delete-btn">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('styles')

@endsection
@section('scripts')
<script>
    $(()=>{
        $('.delete-btn').on('click',function(){
            let id = $(this).attr('id');
            deleteItem('products',id,);
        });
    })
</script>
@endsection
