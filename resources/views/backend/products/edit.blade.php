@extends('backend.layouts.master')
@section('page-nav')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Ürün
                        <small>Düzenle</small>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}" class="text-orange nav__link">Anasayfa</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.products.index')}}" class="text-orange nav__link">Liste</a></li>
                        <li class="breadcrumb-item active">Düzenle</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- ./row -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-olive card-outline card-tabs">
                        <!-- /.general -->
                        <div class="card-body">
                            <form action="{{route('admin.products.update',$product->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label>Adı</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control" name="title" value="{{$product->title}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Fiyat</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="number" min="0" step="any" class="form-control" name="price" value="{{$product->price}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Açıklama</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <textarea id="body" name="body" class="form-control editors">{!! $product->body !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mevcut Görsel</label>
                                    <div class="row">
                                        <div class="col-12">
                                            <img width="150" src="{{$product->image}}" alt="img">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Görsel<span class="text-danger small">(Türü: jpeg, png, jpg, gif, svg olmalı ve Max: 1MB olmalıdır.)</span></label>
                                    <div class="row">
                                        <div class="col-12">
                                            <input id="image" type="file" name="image" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-image mb-3 text-center">
                                    <div class="imgPreview"> </div>
                                </div>
                                <hr>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-olive">Kaydet</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('styles')
    <style>
        .imgPreview img{max-height: 150px; border:1px solid #ddd; margin:5px;}
    </style>
@endsection
@section('scripts')
    <script>
        $(function() {
            // Multiple images preview with JavaScript
            var multiImgPreview = function(input, imgPreviewPlaceholder) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#image').on('change', function() {
                $('.imgPreview').empty();
                multiImgPreview(this, 'div.imgPreview');
            });
        });
    </script>
@endsection

