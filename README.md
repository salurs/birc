# Test Proje

 ## Gereklilikler ve Versiyonlar

    - Php 7.4
    - Laravel Framework Verison 8.4*
    - Composer
    - Git

 ## Kurulum
    - git clone <repoUrl> (Bitbucket'taki proje çekilecek.)
    - composer install (Paket ve bağımlılıklar kurulacak.)
    - .env dosyası eklenecek(Gerekli ayarlamalar yapılacak).
    - php artisan migrate (Tablolar oluşturulacak)
    - php artisan db:seed (Varsayılan veriler yazılacak)
    - Varsayılan Admin (email admin@admin.com, password 12344321)
    - Varsayılan Kullanıcı (email user@user.com, password 12344321)
    
 ## Diğer
