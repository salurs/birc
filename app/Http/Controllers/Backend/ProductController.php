<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('backend.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'price'=>'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $product = new Product;
        $path = '';
        if($request->hasFile('image')){
            $path = '/storage/'.$request->image->store('uploads/products','public');
        }
        $product->title = $request->title;
        $product->slug = Str::slug($request->title);
        $product->body = $request->body;
        $product->price = $request->price;
        $product->image = $path;
        $is_save = $product->save();
        if($is_save){
            return redirect(route('admin.products.index'))->with('success','İşlem Başarılı');
        }
        return back()->with('error','İşlem Başarısız');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('backend.products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'price'=>'required|numeric',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $product = Product::find($id);

        $oldFile = '';
        if($request->hasFile('image')){
            $oldFile = $product->image;
            $path = '/storage/'.$request->image->store('uploads/products','public');
            $product->image = $path;
        }
        $product->title = $request->title;
        $product->slug = Str::slug($request->title);
        $product->body = $request->body;
        $product->price = $request->price;
        $is_save = $product->save();
        if($is_save){
            if (Storage::disk('public')->exists($oldFile)) {
                Storage::disk('public')->delete($oldFile);
            }
            return redirect(route('admin.products.index'))->with('success','İşlem Başarılı');
        }
        return back()->with('error','İşlem Başarısız');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find(intval($id));
        $file = $product->file;
        if ($product->delete()){
            if (Storage::disk('public')->exists($file)) {
                Storage::disk('public')->delete($file);
            }
            return true;
        }
        return false;
    }
}
