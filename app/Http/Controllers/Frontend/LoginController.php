<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('frontend.auth.login');
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('home'));
        }

        return back()->with('error', 'Hatalı email yada şifre');
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('home'))->with('success', 'Güvenli Çıkış Yaptınız.');
    }
}
