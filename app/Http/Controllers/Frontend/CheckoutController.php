<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Cart;
use App\Models\Address;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    public function store(Request $request)
    {
        $user = auth()->user();
        $cart = $user->getCart();
        $cartTotal = $cart->sum('sub_total');

        if ($cart->count() <= 0) {
            return redirect()->home();
        }
        $address_id = $request->address_id;
        if($request->new_address == 'on'){
            $address = $user->addresses()
                ->create($request->only(['title', 'name', 'email', 'phone', 'address']));
            $address_id = $address->id;
        }


        $order = $user->orders()
            ->create([
                'address_id' => $address_id,
                'order_no' => Str::random(10),
                'total' => $cartTotal,
                'payment_type' => $request->payment_type
            ]);

        foreach ($cart as $item) {
            $order->orderItems()
                ->create([
                    'product_name' => $item->product->title,
                    'product_id' => $item->product->id,
                    'quantity' => $item->quantity,
                    'price' => $item->product->price
                ]);

            $item->delete();
        }

         return redirect()->route('checkout.result')->with('order_no', $order->order_no);
    }
}
