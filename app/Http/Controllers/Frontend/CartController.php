<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'product' => 'required',
            'quantity' => 'required',
        ]);

        $productQuantity = Cart::whereProductId($request->product)
            ->select('quantity')
            ->value('quantity');

        Cart::updateOrCreate([
            'product_id' => $request->product
        ], [
            'user_id' => auth()->id(),
            'quantity' => $productQuantity + $request->quantity
        ]);

        return back()->with('success', 'Success');
    }

    public function remove(Cart $cart)
    {
        $cart->delete();

        return back()->with('success', 'Success');
    }

    public function update(Cart $cart)
    {
        request()->validate([
            'quantity' => 'required'
        ]);

        $cart->update([
            'quantity' => request('quantity', 1)
        ]);

        return response()->json(['status' => 'success', 'sub_total' => $cart->getSubTotal()]);
    }
}
