<?php

function getPaymentType($type){
    if($type == 1)
        return 'Kapıda Ödeme';
    else if ($type == 2)
        return 'Kredi Kartı';
    return 'Diğer';
}
function setDateFormat($date){
    $d = \Carbon\Carbon::parse($date)->isoFormat('DD-MM-YYYY');
    return $d;
}

function getOrderStatus($status){
    if($status == 1)
        return 'Onaylandı';
    return 'Beklemede';
}
