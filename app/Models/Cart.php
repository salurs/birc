<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'product_id', 'quantity'];

    public static function booted()
    {
        static::addGlobalScope('user', function ($query) {
            $query->whereUserId(auth()->id());
        });
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getSubTotal()
    {
        return $this->quantity * $this->product->price;
    }
}
