<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $cart = collect();
            $cartTotal = 0;

            if (auth()->check()) {
                $cart = auth()->user()->getCart();
                $cartTotal = $cart->sum('sub_total');
            }

            $view->with(compact('cart', 'cartTotal'));
        });
    }
}
